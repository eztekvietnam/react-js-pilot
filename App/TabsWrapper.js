
import React from 'react';
import TabbedArea from 'react-bootstrap/lib/TabbedArea';
import TabPane from 'react-bootstrap/lib/TabPane';
import ReactEvents from 'react-events';

var TabsWrapper = React.createClass({
    getInitialState(){
        return {
            currentTab: 1
        };
    },
    handleSwitchTab(tabKey) {
        this.setState({currentTab: tabKey});

        if (this.props.onTabChanged)
            this.props.onTabChanged(tabKey);
    },
    render() {
        return (
            <TabbedArea bsStyle='tabs' activeKey={this.state.currentTab} onSelect={this.handleSwitchTab}>
                {
                    this.props.children.map((item) => {
                        return (
                            <TabPane eventKey={item.ref} key={item.ref} tab={item.props.headingTitle}>
                                {item}
                            </TabPane>
                        );
                    })
                }
            </TabbedArea>
        );
    }
});

module.exports = TabsWrapper;