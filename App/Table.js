import React from 'react';
import DataComponents from 'react-data-components';
import CustomizedDataTable from './CustomizedDataTable';

var Table = React.createClass({
    displayName: 'Table',
    propTypes: {
        keys: React.PropTypes.array,
        columns: React.PropTypes.array,
        pageLengthOptions: React.PropTypes.array,
        data: React.PropTypes.array,
        pageLength: React.PropTypes.number,
        sortProp: React.PropTypes.string,
        sortOrder: React.PropTypes.string
    },
    getDefaultProps() {
        return {
            pageLengthOptions: [5, 20, 50],
            sortOrder: 'ascending',
            sortProp: '',
            pageLength: 5,
            columns: [],
            keys: [],
            data: []
        };
    },
    render() {
        return (
            <CustomizedDataTable
                className="wrapper-big-table"
                keys={this.props.keys}
                columns={this.props.columns}
                initialData={this.props.data}
                initialPageLength={this.props.pageLength}
                initialSortBy={{ prop: this.props.sortProp, order: this.props.sortOrder }}
                pageLengthOptions={this.props.pageLengthOptions}
                />);
    }
});

module.exports = Table;