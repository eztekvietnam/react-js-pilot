'use strict';

import React  from 'react';
import Table  from 'react-data-components/lib/Table'
import Pagination  from 'react-data-components/lib/Pagination';

import DataMixin from 'react-data-components/lib/DataMixin';

var CustomizedDataTable = React.createClass({
    displayName: "CustomizedDataTable",

    mixins: [DataMixin],

    render: function () {
        var page = this.buildPage();
        var state = this.state;
        var start = state.pageLength * state.currentPage;
        var end = start + state.pageLength;
        var totalEntries = state.data.length;
        return (
            <div className={this.props.className}>
                <Table className={'table table-bordered'}
                       dataArray={ page.data}
                       columns={this.props.columns}
                       keys={this.props.keys}
                       sortBy={this.state.sortBy}
                       onSort={this.onSort}
                    />

                <div className="row">
                    <div className="col-xs-6">
                        Showing {start + 1} to {end} of {totalEntries} entries
                    </div>
                    <div className="col-xs-6">
                        <Pagination className="pagination grey pull-right"
                                    currentPage={page.currentPage}
                                    totalPages={page.totalPages}
                                    onChangePage={this.onChangePage}
                            />
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = CustomizedDataTable;
