import React from 'react';
import Table from './Table';
import $ from 'jquery';
import ModalTrigger from 'react-bootstrap/lib/ModalTrigger';
import ConfirmationBox from './ConfirmationBox';
import AjaxService from './AjaxService';

var ReceivedOffers = React.createClass({
    displayName: 'ReceivedOffers',
    ajaxService: new AjaxService(),
    propTypes: {
        headingTitle: React.PropTypes.string.isRequired,
        requestUrl: React.PropTypes.string.isRequired,
        columns: React.PropTypes.array
    },
    getInitialState(){
        return {
            data: []
        }
    },
    componentWillMount: function () {
        this.loadData();
    },
    getDefaultProps(){
        return {
            headingTitle: "",
            requestUrl: "",
            columns: []
        }
    },
    loadData()  {
        var self = this;
        if (!self.props.requestUrl)
            throw new Error("RequestURL is mandatory");

        this.ajaxService.execute({
            url: self.props.requestUrl,
            type: 'GET'
        }).then((data) => {
            var decorated = self.decorateData(data);
            self.setState({
                data: decorated,
                responseData: data
            });
        }, (error => {
            console.log(error);
        }));
    },
    decorateData(data){
        return data['offers-received'].items.map((record) => {
            var newRecord = {
                id: record['id'],
                projectName: record['project-name'],
                owner: record['owner'],
                budget: record['budget']['formatted'],
                dateOffered: record['date-offered']['formatted'],
                action: ''
            };

            return newRecord;
        });
    },
    renderActionCellCallback (value, row) {
        var acceptUrl = this.state.responseData['offers-received']['accept-url'];
        var modal = (
            <ConfirmationBox confirmationTitle='Delete offer confirmation'
                             confirmationMsg={'Are you sure want to delete offer ' + row.projectName + '?'}
                             onRequestConfirmed={this.deleteConfirmed.bind(this, row)}
                             onRequestCancelled={this.deleteCancelled}
                             triggerTitle="Delete"
                             triggerClassName="btn btn-link"
                />
        );
        return (
            <div className='text-center'>
                <a href={acceptUrl + '/' + row.id}>Accept</a> | {modal}
            </div>);
    },
    deleteConfirmed (record){
        console.log('Delete confirmed', record);
        var deleteUrl = this.state.responseData['offers-received']['delete-url'];
        var self = this;
        this.ajaxService.fakeExecute({
            url: deleteUrl + '/' + record.id,
            type: 'DELETE',
            result: "Delete success"
        }).then((result) => {
            console.log("Delete success, message: ", result);
            console.log("calling refresh data");
            self.loadData();
        }, (error) => {
            console.error(error);
        });
    },
    deleteCancelled(){
        console.log('Delete cancelled');
    },
    render() {
        var columns = [
            {title: 'Project Name', prop: 'projectName'},
            {title: 'Owner', prop: 'owner', className: 'text-center'},
            {title: 'Budget', prop: 'budget', className: 'text-center'},
            {title: 'Date Offered', prop: 'dateOffered', className: 'text-center'},
            {title: 'Action', render: this.renderActionCellCallback, className: 'text-center'}
        ];

        return (
            <div className="">
                <h3 className="h3 gray bold">Offers Received</h3>

                <div className="clearFix"/>
                <Table columns={columns}
                       sortProp={'projectName'}
                       keys={['projectName','owner']}
                       data={this.state.data}/>
            </div>
        );
    }
});

module.exports = ReceivedOffers;