
import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import OverlayMixin from 'react-bootstrap/lib/OverlayMixin';
import ModalTrigger from 'react-bootstrap/lib/ModalTrigger';

const ConfirmationBox = React.createClass({
    displayName: 'ConfirmationBox',
    mixins: [OverlayMixin],
    propsType: {
        triggerTitle: React.PropTypes.string.required,
        confirmationTitle: React.PropTypes.string.required,
        confirmationMsg: React.PropTypes.string.required,
        onRequestConfirmed: React.PropTypes.func,
        onRequestCancelled: React.PropTypes.func
    },
    getInitialState() {
        return {
            isModalOpen: false
        };
    },

    handleToggle() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    },
    onRequestConfirmed(){
        this.props.onRequestConfirmed();
        this.setState({
            isModalOpen: !1
        });
    },
    onRequestCancelled(){
        this.props.onRequestCancelled();
        this.setState({
            isModalOpen: !1
        });
    },
    render() {
        return (
            <a onClick={this.handleToggle} className="btn-link">
                {this.props.triggerTitle}
            </a>
        );
    },
    renderOverlay() {
        if (!this.state.isModalOpen) {
            return <span/>;
        }

        return (
            <Modal bsStyle='primary'
                   title={this.props.confirmationTitle}
                   animation={true}
                   onRequestHide={this.handleToggle}>
                <div className='modal-body'>
                    {this.props.confirmationMsg}
                </div>
                <div className='modal-footer'>
                    <button onClick={this.onRequestConfirmed} className="btn btn-danger">Yes</button>
                    <button onClick={this.onRequestCancelled} className="btn btn-default">No</button>
                </div>
            </Modal>
        );
    }
});

module.exports = ConfirmationBox;