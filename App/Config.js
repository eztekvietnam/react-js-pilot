
var Config = {
    sentOffersGetRequestUrl: 'sentOffers.json',
    receivedOffersGetRequestUrl: 'receivedOffers.json'
};

module.exports = Config;