
import $ from 'jquery';
import Q from 'q';

class AjaxService {
    constructor(ajaxServiceImpl) {
        this.ajaxImpl = ajaxServiceImpl || $.ajax;
    }

    fakeExecute(params) {
        function fakeAjaxRequest() {
            var defer = Q.defer();
            if (params.result)
                defer.resolve(params.result);
            else
                defer.reject("No response from server");

            return defer.promise;
        }

        // execute real request -- no matter if it's fail or not cuz we will return the fake
        this.ajaxImpl(params);
        return Q.fcall(fakeAjaxRequest.bind(this));
    }

    execute(params) {
        return Q.fcall(this.ajaxImpl.bind(this, params));
    }
}

module.exports = AjaxService;