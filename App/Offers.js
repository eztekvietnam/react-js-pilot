
import React from 'react';
import TabsWrapper from './TabsWrapper';
import SentOffers from './SentOffers';
import ReceivedOffers from './ReceivedOffers';
import Config from './Config';

var Offers = React.createClass({
    propTypes: {
        config: React.PropTypes.object
    },
    getDefaultProps(){
        return {
            config: {}
        }
    },
    onTabChanged(tabKey){
        // refresh table when tab changed
        this.refs[tabKey].loadData();
    },
    render() {
        return (
            <TabsWrapper onTabChanged={this.onTabChanged}>
                <ReceivedOffers headingTitle={"Received Offers"}
                                ref={1}
                                requestUrl={this.props.config.receivedOffersGetRequestUrl}/>
                <SentOffers headingTitle={"Sent Offers"}
                            ref={2}
                            requestUrl={this.props.config.sentOffersGetRequestUrl}/>
            </TabsWrapper>
        );
    }
});

React.render(<Offers config={Config}/>, document.getElementById('content'));


