*Prerequisites* 

- npm i -g browserify

- npm i -g babel


*To install dependencies*

- npm i


*To build*

- browserify -t babelify App\Offers.js -o dist\main.js


*To run*

- Open index.html in browser. 


In order to be able to load the json files with files:// protocol, Chrome need to be started with --disable-web-security, or the files to be served under a virtual web server